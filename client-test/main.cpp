#include <iostream>
#include "square.h"

using namespace std;

int main() {
	shape::Square s(5);
	cout << "Area: " << s.getArea() << endl;
	return 0;
}
