#include "square.h"

using namespace shape;

Square::Square(int p_side) 
    : side (p_side)
{}

int Square::getArea() const {
    return side * side;
}