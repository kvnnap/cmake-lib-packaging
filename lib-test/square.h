#pragma once

namespace shape {
    class Square {
    public:
        Square(int p_side);
        int getArea() const;
    private:
        int side;
    };
}